/*
 * Copyright (c) 2018-2020 Viktor Kireev
 * Distributed under the MIT License
 */

#include <otn/all.hpp>

#include <iostream>

void consumeUnique(otn::unique_optional<std::string> unique_string)
{
    using namespace std;

    if (unique_string)
    {
        cout << "Consume the unique string: " << *unique_string << endl;
        otn::utilize(std::move(unique_string));
    }
    else
    {
        cout << "The unique string is empty" << endl;
    }

    assert(!unique_string);
}

int main()
{
    using namespace std;
    using namespace otn;

    unique_optional<string> hello_unique{itself, "Hello, World!"};
    weak_optional<string>   hello_weak = hello_unique;

    cout << "Initial unique string: " << *hello_unique << endl;

    {
        // Prolong the lifetime of the string from the hello_unique in this scope.
        unified_optional<string> hello_unified = hello_weak;

        consumeUnique(std::move(hello_unique));

        assert(!hello_unique);
        assert(hello_unified);
        assert(!hello_weak.expired());

        cout << "The unique string is empty." << endl;
        otn::access(hello_weak, [](const auto& hello_weak)
        {
            cout << "But the weak string is still alive: "
                 << hello_weak << endl;
        });
    }

    assert(hello_weak.expired());
    cout << "And here the weak string has expired." << endl;

    return 0;
}
